E3 uwu


Projet : Supervision
============

**Introduction :**

Le but de ce projet est de créer une application de supervision avec Python et un Arduino.
Il y a déjà une ébauche, mais chacun peut écrire des fonctions pour améliorer ce programme.
L’arduino mesure 4 température et contrôle des relais.
 Le python est relié en liaison serie à un Rasberry pi ou un PC.
Le python affiche les température les différents temps et contrôle les relais en fonction de la température.
Le programme est articulé en fonction comme actualiser les températures ou créer un afficheur.
Il y a donc une boucle d’initialisation et une boucle infinie. Cette architecture doit être maintenue.
Le programme python est fournie avec des fonctions factices pour pouvoir tester le programme sans l’arduino. On peut les désactiver simplement en remplacent ces fonctions par la fonction du même nom sans le factice dans Loop et initialiser, et de retirer le # devants connecter. De cette manière, on connecte l’arduino et on réalise les bonnes fonctions. De même l’arduino possède une fonction factice pour pouvoir se passer de vrai sonde de température et mieux tester le programme.


**Travail à réaliser :**

Vous pouvez choisir n’importe quelle tache dans la liste, et me le dire de manière à synchroniser nos efforts et vous intégrer dans le groupe Messenger.

   - Ecrire une fonction pour afficher l’heure du PC. Pour cela on doit :
	 * Ecrire une fonction créer_horloge
	 * Ecrire une fonction actualiser_horloge
	 * modifier actualiser_chronometre pour que le chrono soit basé sur le temps système.
   - Se passer de variable globale :
	 * Ce n’est pas sûre que ce soit possible.
   - Utiliser le Bluetooth du rasberry et relier la supervision à une application mobile.
	 * Faire suggestion d’interface à l’avance.
	 * garder l’architecture en Loop infinie.
   - Rendre l’interface plus jolie.
	 * Faire suggestion
   - Ecrire la doc du programme :
	 * Voir le modèle type à utiliser chez Uwu.
   - Créer un menu des recettes :
	 * La liste des temps et chrono n’est unique. Il faut donc à l’étape d’initialisation demander à l’utilisateur quelle recette il veut suivre.
	 * Pouvoir ajouter, retirer et modifier les recettes.
	 * afficher la recette en cours
   - Transformer le python en un Exe ou un fichier exécutable linux.
   - Tester et debugger l’application sur la vraie maquette :
	 * Pas si facile d’accès c’est pour cela qu’on a les fonctions factices. 
   - Ajouter un compteur d’eau :	
	 * On peut relier l’arduino à un compteur d’eau (un contacteur avec un contacte à chaque litre).
	 * Optimiser l’arduino pour ne pas rater de litre.
	 * Ecrire une fonction créer_compteur_eau.
	 * Ecrire une fonction actualiser_compteur_eau.
   - Suggérer vos idées pour améliorer la supervision, la rendre plus simple d’utilisation, avec plus d’options.
