
#include <OneWire.h> //bibliotheque onewire permet de communiquer avec la sonde de tempèrature
#include <DallasTemperature.h> //bibliotheque DallasTemperature permet de communiquer avec la sonde de tempèrature

//les pin 0 et 1 sont utlisé par le port série ils sont donc occupés
#define ONE_WIRE_BUS1 2 //designe le port 2 pour communiquer avec le protocole onewire
OneWire oneWire1(ONE_WIRE_BUS1); 
DallasTemperature sensors1(&oneWire1);//indique que a sonde nomée sensors1 communique via onewire1 declaré au dessus

#define ONE_WIRE_BUS2 3 //etc
OneWire oneWire2(ONE_WIRE_BUS2); 
DallasTemperature sensors2(&oneWire2);

#define ONE_WIRE_BUS3 4 
OneWire oneWire3(ONE_WIRE_BUS3); 
DallasTemperature sensors3(&oneWire3);

#define ONE_WIRE_BUS4 5 
OneWire oneWire4(ONE_WIRE_BUS4); 
DallasTemperature sensors4(&oneWire4);




int T1 = 50;//déclare des variable pour memoriser les temperatures
int T2 = 50;
int T3 = 50;
int T4 = 50;
int temp;

void setup() {//bouvle d'initialisation
  Serial.begin(9600);//initialise le port serie avec le paramètre 9600(information à utiliser dans le python
  pinMode(8, OUTPUT);//déclare les pin utilisé pour les relais (déclaration traditionnelle)
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

}

void loop() {//boucle infinie du programme
  lire_temperature();//appel de la fonction lire_temperature
  envoyer();//appel de la fonction  envoyer
  relais();//appel de la fonction relay
  delay(500);// pausse de 500ms (on ne fait rien pendant 500ms


}

void envoyer(){//fonction pour envoyer le message les temperatures au python
  Serial.print( 'd');//ecrit sur le port série un message du type:'d21t28t103t14f  avec d pour le debut, f la fin, et t entre les temperatures
  Serial.print(T1);
  Serial.print( 't');
  Serial.print(T2);
  Serial.print( 't');
  Serial.print(T3);
  Serial.print( 't');
  Serial.print(T4);
  Serial.print( 'f'); 
}


void lire_temperature(){//permet de lire les températures sur les sondes
   sensors1.requestTemperatures();
   T1=sensors1.getTempCByIndex(0);
   sensors2.requestTemperatures();
   T2=sensors2.getTempCByIndex(0);
   sensors3.requestTemperatures();
   T3=sensors3.getTempCByIndex(0);
   sensors4.requestTemperatures();
   T4=sensors4.getTempCByIndex(0);
  
}


void relais(){//active les relais en fonction de la lettre envoyé par le python
  while (Serial.available() > 1) {//lit seulement si il y a un message en memoire
    
    


    
      temp=Serial.read();
      if (temp==97){// dans la table ascii le a correspond au 97
        digitalWrite(8, HIGH);
      }
      if (temp==98){
        digitalWrite(8, LOW);
      }
      if (temp==99){
        digitalWrite(9, HIGH);
      }
      if (temp==100){
        digitalWrite(9, LOW);
      }
      if (temp==101){
        digitalWrite(10, HIGH);
      }
      if (temp==102){
        digitalWrite(10, LOW);
      }



      
  }
      
  
}
