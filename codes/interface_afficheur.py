from tkinter import *
import serial
from time import sleep

#Variables globales
i=0#numéro de l'étape en cours
afficheurs=0#variable pour stocker les listes des dessins
Mesures=[50,50,50,50,50]#Valeurs des temperatures en mémoire
ser=0#varible pour stocker le port serie
pause=1#varible qui indique la pause
relais=[0,0,0]#liste des etats des relais



def lire_mesure_factice():#fonction qui donne une mesure de température factice

    global Mesures#permet de modifier la variable globale

    t1=Mesures[0]#transforme la liste pour faciliter la lecture
    t2=Mesures[1]
    t3=Mesures[2]
    t4=Mesures[3]
    r=[4]


    t1+=1#change la temperature
    t2-=1
    t3+=2
    t4+=1.5

    t1=t1%100#temperature reste comprise entre 0 et 100°C
    t2=t2%100
    t3=t3%100
    t4=t4%100


    Mesures=[t1,t2,t3,t4,r]#memorise la temperature


def connecter():#permet de connecter
    global ser#permet de modifier la variable globale
    ser = serial.Serial(port="COM3", baudrate=9600, timeout=1, writeTimeout=1)#connection au port serie avec les parametre récupérés dans le logiciel arduino
    ser.write('connection'.encode('ascii'))#ecrit sur le port serie


def etat_relais():#modifie les etat du relais (0 ou 1) en fonction de la temperature
    if Mesures[2] < afficheurs[2][4]-afficheurs[2][5]:
        relais[0]=1
        relais[1]=0
    elif Mesures[2] > afficheurs[2][4]+afficheurs[2][5]:
        relais[0]=0
        relais[1]=1
    else:
        relais[0]=0
        relais[1]=0
    if Mesures[3] > afficheurs[3][4]+afficheurs[3][5]:
        relais[2]=0
    else:
        relais[2]=1



def creer_afficheur_relais(dessin,x,y,h,px,py):#creer un cadre avec les dimenssions indiquées et un texte qui donne l'etat des relais
    l=6*h
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.5)*py,font=("arial",int(0.5*h*py)), text=('R1=',relais[0],'R2=',relais[1],'R3=',relais[2]))

    L=[aff,dessin]#memorise les infos utiles pour modifier le texte
    return L



def actualiser_afficheur_relais(L):#permet de modifier le texte dans le cadre
    aff=L[0]
    dessin=L[1]
    dessin.itemconfigure(aff,text=('R1=',relais[0],'R2=',relais[1],'R3=',relais[2]))



def creer_titre(dessin,px,py):#creer 4 cadres avec les titres
    x=0.05
    y=0.04
    h=0.05
    l=4.5*h
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.5)*py,font=("arial",int(0.5*h*py)), text=('Temperature 1'))
    x=0.275
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.5)*py,font=("arial",int(0.5*h*py)), text=('Temperature 2'))
    x=0.5
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.5)*py,font=("arial",int(0.5*h*py)), text=('Temperature 3'))
    x=0.725
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.5)*py,font=("arial",int(0.5*h*py)), text=('Temperature 4'))



def lire_mesure():#permet de lire le message sur le port serie
    global Mesures
    donnee_brute=str(ser.readline())#lit le le message stocké sur le port serie
    donnee=donnee_brute[2:-1]#sépare le message des autres caractère comme: "
    T=['','','','']#variable temporaire pour stocker les temperatures
    d=0#compteur qui indique la position du caractère à lire
    c=0#compteur qui indique la température à mémoriser (1,2,3ou 4)
    f=len(donnee)-1#longeur du message
    if f>=0:#si le message n'est pas vide

        while donnee[d]!='d'and d<f:#cherhce le caractère d qui marque le dèbut du message
            d+=1
        d+=1
        while d<f and donnee[d]!='f':#on balaye le message caractère par caractère
            if donnee[d]=='t':#si le carctère est t cela indique que l'on passe à la mesure de temperature suivante
                c+=1
            else:
                T[c]=T[c]+donnee[d]#ajoute le caractère à la mesure en cours de mesure
            d+=1

        if T[0]!='':
            Mesures[0]=int(T[0])#transforme la liste de carcatère stokée dans T en temperature dans Mesures
        if T[1]!='':
            Mesures[1]=int(T[1])
        if T[2]!='':
            Mesures[2]=int(T[2])
        if T[3]!='':
            Mesures[3]=int(T[3])


def envoyer_messsage():#permet d'envoyer un message avec les etats consignes des relais
    global relais

    if relais[0]:
        ser.write('a'.encode('ascii'))#si le relais 1 a pour valeur 1 on ecrit 'a' sur le port serie
    else:
        ser.write('b'.encode('ascii'))#etc
    if relais[1]:
        ser.write('c'.encode('ascii'))
    else:
        ser.write('d'.encode('ascii'))
    if relais[2]:
        ser.write('e'.encode('ascii'))
    else:
        ser.write('f'.encode('ascii'))





#def envoyer_consigne

#def mode_hors_service#a faire faire

#def afficher_cadran # a faire faire

#lire entrée clavier





def creer_afficheur_vertical(dessin,x,y,l,h,max,min,px,py,consigne,marge):#creer un afficheur vertcale avec les valeurs consiges
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")#rectangle gris pour le delimiter
    dessin.create_rectangle((x+l*0.25)*px,(y+h*0.1)*py,(x+l*0.75)*px,(y+h*0.9)*py ,fill="white")#rectaangle blanc pour le fond de l'afficheur
    rectangle=dessin.create_rectangle((x+l*0.25)*px,(y+h*0.1+0.25*0.8*h)*py,(x+l*0.75)*px,(y+h*0.9)*py ,fill="blue")#rectangle qui indique la hauteur de la temperature
    dessin.create_line(x*px,(y+h*0.1+((max-consigne)/(max-min))*0.8*h)*py,(x+l)*px,(y+h*0.1+((max-consigne)/(max-min))*0.8*h)*py,fill="red")#ligne du niveau de la consigne
    dessin.create_line(x*px,(y+h*0.1+((max-consigne-marge)/(max-min))*0.8*h)*py,(x+l)*px,(y+h*0.1+((max-consigne-marge)/(max-min))*0.8*h)*py,fill="black")#ligne du niveau de la consigne avec la marge
    dessin.create_line(x*px,(y+h*0.1+((max-consigne+marge)/(max-min))*0.8*h)*py,(x+l)*px,(y+h*0.1+((max-consigne+marge)/(max-min))*0.8*h)*py,fill="black")#ligne du niveau de la consigne avec la marge

    L=[rectangle,0.8*h*py,max,min,consigne,marge,dessin]#recupère les informations utiles pour modifier l'afficheur
    print(consigne)
    return L




def maj_afficheur_verticale(L,T):#actualiser l'afficheur verticale
    rectangle=L[0]#transforme les donnée de la liste pour faciliter la lecture
    h=L[1]
    max=L[2]
    min=L[3]
    consigne=L[4]
    marge=L[5]
    dessin=L[6]
    x1,y1,x2,y2=dessin.coords(rectangle)#recupère les coordonnées actuelle
    if ((T<(max))and(T>(min))):#si la temperature depasse les extremum alors on ne l'affiche pas
        y1=(y2-h*((T-min)/(max-min)))#sinon on redimenssion le rectangle pour afficher la nouvelle temperature
        dessin.coords(rectangle,x1,y1,x2,y2)
        if ((T>(consigne+marge))or(T<(consigne-marge))):#on change la couleur de la barre si la temperature sort des marges de tolèrences
            dessin.itemconfig(rectangle,fill="red")
        else:
            dessin.itemconfig(rectangle,fill="green")
    else:
        dessin.itemconfig(rectangle,fill="black")






def creer_afficheur_numerique(dessin,x,y,h,max,min,px,py,consigne,marge):#affiche la temperature avec un nombre
    l=3*h
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")#creer un rectangle gris
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.33)*py,font=("arial",int(0.5*h*py)), text=(max,"°C"))#creer une zone de texte pour la temperature
    dessin.create_text((x+l*0.5)*px,(y+h*0.80)*py,font=("arial",int(0.2*h*py)), text=(consigne,"°C"))#creer une zone de texte pour la consigne
    L=[aff,max,min,consigne,marge,dessin]#recupère les informations utiles pour modifier l'afficheur
    return L




def maj_afficheur_numerique(L,T):#permet d'actualiser la temperature affichée
    aff=L[0]#rend la liste plus lisible
    max=L[1]
    min=L[2]
    consigne=L[3]
    marge=L[4]
    dessin=L[5]
    dessin.itemconfigure(aff, text=(T,"°C"))#change le texte
    if ((T>(consigne+marge))or(T<(consigne-marge))):#adapte la couleur en fonction de la consigne
        dessin.itemconfigure(aff,fill="red")
    else:
        dessin.itemconfigure(aff,fill="black")






def creer_chronometre(dessin,x,y,h,px,py,tps):#creer un chronometre pour afficher le temps restant
    l=3*h
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")#rectangle gris
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.5)*py,font=("arial",int(0.5*h*py)), text=(tps,"s"))#zone de texte

    L=[aff,tps,dessin]#recupère les informations utiles pour modifier l'afficheur
    return L




def actualiser_chronometre(L):#actualise le temps affiché
    aff=L[0]#rend la liste plus lisible
    tps=L[1]
    dessin=L[2]
    tps-=0.5
    if tps<0:#si le temps est négatif alors on passe en rouge pour indiquer le de combien on a depasser le temps consigne
        dessin.itemconfigure(aff,text=(tps,"s"),fill="red")
    else:
        dessin.itemconfigure(aff,text=(tps,"s"),fill="black")#sinon on affiche en noir le temps qui nous reste

    return tps





def creer_afficheur_etape(dessin,x,y,h,px,py):#affiche l'etape en cours
    l=3*h
    dessin.create_rectangle(x*px,y*py, (x+l)*px,(y+h)*py ,fill="lightgrey")#rectangle gris
    aff=dessin.create_text((x+l*0.5)*px,(y+h*0.5)*py,font=("arial",int(0.5*h*py)), text=("etape:",i))#zone de texte avec i qui est une variable globale

def depart():#modifie la pause pour demarer le chronometre
    global pause
    pause=0

def etape_suivante(dessin,px,py):
    clean(dessin)#efface l'ecran
    global i
    global pause
    pause=1#passe le temps en pause

    if i!="fin":
        i+=1#indente le numero de l'etape
    para=etape(i)
    creer_affichage(px,py,dessin,para)#affiche les nouveaux afficheurs pour l'etape i


def etape_precedente(dessin,px,py):#idem mais avec l'etape precedente
    clean(dessin)
    global i
    global pause
    pause=1

    i-=1
    para=etape(i)
    creer_affichage(px,py,dessin,para)




def etape(I):#donne les paramètres de l'etape i
    global i
    if I=="fin":#si l'étape est 'fin' alors on revient au debut
        para=[[100,0,50,10],[100,0,50,10],[100,0,50,10],[100,0,50,10],0]#parametre des 4 afficheur (max,min,consigne,marge) et du temps
        i=0
        return(para)
    if I<1:#paramètre de l'étape 0
        para=[[100,0,50,10],[100,0,50,10],[100,0,50,10],[100,0,50,10],0]
        return(para)


    if I==1:#paramètre de l'étape 1
        para=[[100,0,50,10],[100,0,50,10],[100,0,50,10],[100,0,50,10],140]
        return(para)
    if I==2:#paramètre de l'étape 2
        para=[[100,0,30,10],[100,0,50,10],[100,0,50,10],[100,0,50,10],150]
        return(para)



    else:#si on a terminé la procédure on affiche stocke fin dans i
        para=[[100,0,80,10],[100,0,50,10],[100,0,50,10],[100,0,50,10],0]
        i="fin"
        return(para)






def creer_boutons(px,py,fen,dessin):#creer 4 boutons avec les commandes associées
    B1=Button(fen, text='Echap',anchor=CENTER,bg='lightgrey',command=lambda: echap(fen))#bouton qui active la fonction echap
    B1.place(x=px*0.9,y=px*0.01)

    B2=Button(fen, text='depart',anchor=CENTER,bg='lightgrey',command=lambda: depart())#bouton qui active la fonction depart
    B2.place(x=px*0.55,y=px*0.853)

    B2=Button(fen, text='<',anchor=CENTER,bg='lightgrey',command=lambda: etape_precedente(dessin,px,py))#etc
    B2.place(x=px*0.75,y=px*0.9)

    B2=Button(fen, text='>',anchor=CENTER,bg='lightgrey',font=("arial",int(12)),command=lambda: etape_suivante(dessin,px,py))
    B2.place(x=px*0.85,y=px*0.9)



def creer_fenetre(px,py):#creer la fenetre pour le dessin
    fen=Tk()
    fen.title("interface")
    dessin=Canvas(fen,height=py,width=px)
    dessin.pack()
    dessin.update()
    return(fen,dessin)


def creer_affichage(px,py,dessin,L):#fonction qui active toute les fonction creer...
    max1=L[0][0]
    min1=L[0][1]
    consigne1=L[0][2]
    marge1=L[0][3]
    max2=L[1][0]
    min2=L[1][1]
    consigne2=L[1][2]
    marge2=L[1][3]
    max3=L[2][0]
    min3=L[2][1]
    consigne3=L[2][2]
    marge3=L[2][3]
    max4=L[3][0]
    min4=L[3][1]
    consigne4=L[3][2]
    marge4=L[3][3]
    tps=L[4]
    print(consigne1)





    aff11=creer_afficheur_vertical(dessin,0.12,0.1,0.1,0.3,max1,min1,px,py,consigne1,marge1)#creer un afficheur à la position indquée
    aff21=creer_afficheur_vertical(dessin,0.34,0.1,0.1,0.3,max2,min2,px,py,consigne2,marge2)
    aff31=creer_afficheur_vertical(dessin,0.56,0.1,0.1,0.3,max3,min3,px,py,consigne3,marge3)
    aff41=creer_afficheur_vertical(dessin,0.78,0.1,0.1,0.3,max4,min4,px,py,consigne4,marge4)

    aff12=creer_afficheur_numerique(dessin,0.06,0.41,0.22/3,max1,min1,px,py,consigne1,marge1)
    aff22=creer_afficheur_numerique(dessin,0.28,0.41,0.22/3,max2,min2,px,py,consigne2,marge2)
    aff32=creer_afficheur_numerique(dessin,0.5,0.41,0.22/3,max3,min3,px,py,consigne3,marge3)
    aff42=creer_afficheur_numerique(dessin,0.72,0.41,0.22/3,max4,min4,px,py,consigne4,marge4)


    creer_titre(dessin,px,py)
    chrono=creer_chronometre(dessin,0.05,0.8,0.15,px,py,tps)


    creer_afficheur_etape(dessin,0.65,0.75,0.1,px,py)
    affr=creer_afficheur_relais(dessin,0.5,0.5,0.05,px,py)

    global afficheurs
    afficheurs=[aff11,aff21,aff31,aff41,aff12,aff22,aff32,aff42,chrono,affr]#stocke dans une variable globale les infos utile de chaques afficheurs




def clean(dessin):#permet d'effacer les dessins
    dessin.delete("all")


def initialisation():#fonction dinitialisation
    px=1000#indique les dimenssions de la fenetre. Par la suite les x,y,h,l sont exprimé en fraction de la largeur de la fenetre
    py=px
    fen,dessin=creer_fenetre(px,py)#creer la fenetre

    para=etape(i)#recupère les paramètre de l'etape(i=0) pour l'affichage

    creer_boutons(px,py,fen,dessin)
    creer_affichage(px,py,dessin,para)#affiche avec les paramètre de létape 0

    #connecter()#connecte le port série pour communiquer avec l'arduino

    loop()#declenche la boucle infinie

    fen.mainloop()





def loop():#bloucle infinie
    global afficheurs
    global Mesures

    dessin=afficheurs[8][2]#stocke la variable dessin pour activer les fonction suivante

    if pause==0:#incrémente le temps seulement si on n'est pas en pause
        tps=actualiser_chronometre(afficheurs[8])#actualise le chronomèter
        afficheurs[8][1]=tps#actualise la donnée du temps en mèmoire


    lire_mesure_factice()

    maj_afficheur_verticale(afficheurs[0],Mesures[0])
    maj_afficheur_verticale(afficheurs[1],Mesures[1])
    maj_afficheur_verticale(afficheurs[2],Mesures[2])
    maj_afficheur_verticale(afficheurs[3],Mesures[3])
    maj_afficheur_numerique(afficheurs[4],Mesures[0])
    maj_afficheur_numerique(afficheurs[5],Mesures[1])
    maj_afficheur_numerique(afficheurs[6],Mesures[2])
    maj_afficheur_numerique(afficheurs[7],Mesures[3])



    etat_relais()
    actualiser_afficheur_relais(afficheurs[9])


    #envoyer_messsage()





    sleep(0.5)#attend une demi seconde
    dessin.update()#actualise le dessin
    dessin.after(0, loop)#boucle infinie de la fonction loop




def echap(fen):#permet de terminer l'execution du programme
    fen.destroy()

initialisation()